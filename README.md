In the project directory, you can run:
### `node index.js`

For starting web server, you can run on another terminal:
### `wscat -c ws://localhost:9000`

For login user in web server, you can run:
### `{"type":"login", "name":"Mahima"}`

For Chatting end to end. Login another user and write this on terminal to chat, you can run:
### `{"type":"chat", "senderid":"Mahima", "receiverid":"Mahima1", "message":"hi"}`

For Leaving the Chat, you can run:
### `{"type":"leave"}`

For Getting all User. You can check user controller, you can run thi api url(POST API) in postman:
### `http://localhost:9000/user/GetAllUsers`

For Getting all Online Users. You can check user controller, you can run thi api url(POST API) in postman:
### `http://localhost:9000/user/GetAllOnlineUsers`

For Getting all Chats of User.You can check chat controller, you can run thi api url(POST API) in postman:
### `http://localhost:9000/chat/GetAllChatByUserId`
### `body: senderid`

For Getting all Messages of Chat of Specific User.You can check chat controller, you can run thi api url(POST API) in postman:
### `http://localhost:9000/chat/GetChatMessageofSpecficUser`
### `body: senderId, receiverId`

Note: Postman Collection url
### `https://www.getpostman.com/collections/2b1893015309572c1464`