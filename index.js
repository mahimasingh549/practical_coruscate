const express = require("express");
const WebSocket = require("ws");
const http = require("http");
const app = express();
require("dotenv").config();

var TokenKey = process.env.tokenkey;
var mongouri = process.env.mongouri;
var mongoose = require("mongoose");
//Models
const User = require('./models/tbluser');
const Message = require('./models/tblmessage');

var jwt = require("jwt-simple");

const port = process.env.PORT || 9000;

//initialize a http server
const server = http.createServer(app);

//initialize the WebSocket server instance
const wss = new WebSocket.Server({ server });

let users = {};
User.updateMany({}, { $set: { online: 0 } }).then(function (response) { })
const sendTo = (connection, message) => {
    connection.send(JSON.stringify(message));
};

const sendToAll = (clients, type, { id, name: userName }) => {
    Object.values(clients).forEach(client => {
        if (client.name !== userName) {
            client.send(
                JSON.stringify({
                    type,
                    user: { id, userName }
                })
            );
        }
    });
};

//WebSocket server connection
wss.on("connection", ws => {
    ws.on("message", msg => {
        let data;
        //accept only JSON messages
        try {
            data = JSON.parse(msg);
        } catch (e) {
            console.log("Invalid JSON");
            data = {};
        }
        const { type, name, senderid, receiverid, message } = data;
        switch (type) {
            case "login":
                User.findOneAndUpdate({
                    name: name
                }, {
                    $set: {
                        online: 1
                    }
                }).then(function (response) {
                    if (response) {
                        const loggedIn = Object.values(
                            users
                        ).map(({ id, name: userName }) => ({ id, userName }));
                        users[name] = ws;
                        ws.name = name;
                        ws.id = id;
                        sendTo(ws, {
                            type: "login",
                            success: true,
                            users: loggedIn
                        });
                        sendToAll(users, "updateUsers", ws);
                    } else {
                        var newUserObject = new Object();
                        newUserObject.name = name;
                        newUserObject.online = 1;
                        User.create(newUserObject).then((ResCreateUser) => {
                            const loggedIn = Object.values(
                                users
                            ).map(({ id, name: userName }) => ({ id, userName }));
                            users[name] = ws;
                            ws.name = name;
                            ws.id = id;
                            sendTo(ws, {
                                type: "login",
                                success: true,
                                users: loggedIn
                            });
                            sendToAll(users, "updateUsers", ws);
                        })
                    }
                })
                break;
            case "chat":
                var newChatObject = new Object();
                newChatObject.senderid = senderid;
                newChatObject.receiverid = receiverid;
                newChatObject.message = jwt.encode(message, TokenKey)
                Message.create(newChatObject).then((ResCreateUser) => {
                    const offerRecipient = users[receiverid];
                    sendTo(offerRecipient, {
                        message,
                        receiverid: receiverid
                    });
                })
                break;
            case "leave":
                sendToAll(users, "leave", ws);
                User.updateMany({}, { $set: { online: 0 } }).then(function (response) { })
                break;
            default:
                sendTo(ws, {
                    type: "error",
                    message: "Command not found: " + type
                });
                break;
        }
    });
    ws.on("close", function () {
        delete users[ws.name];
        User.updateMany({}, { $set: { online: 0 } }).then(function (response) { })
        sendToAll(users, "leave", ws);
    });
    // ws.send(
    //     JSON.stringify({
    //         type: "connect",
    //         message: "Well hello there, I am a WebSocket server"
    //     })
    // );
});

//Mongo Db connection
let db = mongoose.connect(mongouri, {
    useNewUrlParser: true,
    useCreateIndex: true
});

//Connection to controller
app.use(express.static(__dirname + "/"));
app.use("/user", require("./controller/user"));
app.use("/chat", require("./controller/chat"));

server.listen(port, () => {
    console.log(`Signalling Server running on port: ${port}`);
});