var express = require("express");
var router = express.Router();
var bodyParser = require("body-parser");
var jsonParser = bodyParser.json();
const User = require("../models/tbluser");


//Get All Users
router.post("/GetAllUsers", jsonParser, function (req, res) {
    User.find().select({ "name": 1, "online": 1, "_id": 0 }).then((resUser) => {
        res.json({
            success: true,
            message: "All Users",
            data: resUser
        }, 200);
    }).catch((error) => {
        res.json({
            success: false,
            message: "Something went wrong!.",
        }, 500);
    })
});

//Get All Online Users
router.post("/GetAllOnlineUsers", jsonParser, function (req, res) {
    User.find({ online: 1 }).select({ "name": 1, "online": 1, "_id": 0 }).then((resOnlineUser) => {
        res.json({
            success: true,
            message: "All Online Users",
            data: resOnlineUser
        }, 500);
    }).catch((error) => {
        res.json({
            success: false,
            message: "Something went wrong!.",
        }, 500);
    })
});

module.exports = router;