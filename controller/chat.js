var express = require("express");
var router = express.Router();
var bodyParser = require("body-parser");
var jsonParser = bodyParser.json();
const User = require("../models/tbluser");
const Message = require("../models/tblmessage");
var u = require("underscore");
var jwt = require("jwt-simple");
var TokenKey = process.env.tokenkey;

//Get All Users Chat by Id
router.post("/GetAllChatByUserId", jsonParser, function (req, res) {
    let objParam = req.body;
    Message.aggregate([
        {
            $match: {
                $or: [
                    {
                        "receiverid": objParam.senderid
                    },
                    {
                        "senderid": objParam.senderid
                    }
                ]
            }
        },
        {
            "$project": {
                senderid: 1,
                receiverid: 1,
                message: 1,
                created_at: 1,
                fromToUser: {
                    $cond: { if: { $eq: ["$senderid", objParam.senderid] }, then: "$receiverid", else: "$senderid" }
                }
            }
        },
        {
            $unwind: "$fromToUser"
        },
        {
            $sort: {
                "fromToUser": -1
            }
        },
        {
            $group: {
                _id: "$_id",
                "fromToUser": {
                    $push: "$fromToUser"
                },
                "senderid": {
                    "$last": "$senderid"
                },
                "receiverid": {
                    "$last": "$receiverid"
                },
                "message": {
                    "$first": "$message"
                },
                "created_at": {
                    "$last": "$created_at"
                }
            }
        },
        {
            "$sort": {
                "created_at": -1
            }
        },
        {
            "$group": {
                "_id": "$fromToUser",
                "fromUser": {
                    "$last": "$senderid"
                },
                "toUser": {
                    "$last": "$receiverid"
                },
                "message": {
                    "$first": "$message"
                },
                "createddate": {
                    "$last": "$created_at"
                }
            }
        },
        {
            $lookup: {
                from: 'tblusers',
                localField: '_id',
                foreignField: 'name',
                as: 'receiverData'
            }
        },
        {
            $unwind: '$receiverData'
        },
        {
            "$project": {
                "message": 1,
                "created_at": 1,
                "receiverData.name": 1,
                "receiverData.online": 1
            }
        },
    ]).then(function (response) {
        if (response.length > 0) {
            u.each(response, (userData) => {
                userData.id = userData._id[0];
                userData.message = jwt.decode(userData.message, TokenKey);
                delete userData._id;
            });
            response.sort((a, b) => b.created_at - a.created_at);
            res.json({
                success: true,
                data: response
            }, 200);
        } else {
            res.json({
                success: false,
                message: "No list found",
                data: []
            }, 404);
        }
    }).catch(function (err) {
        res.json({
            success: false,
            message: err.message
        }, 500);
    });

});

//Get All Chat Messages By sender and receiver Id
router.post("/GetChatMessageofSpecficUser", jsonParser, function (req, res) {
    let objParam = req.body;

    //Pagination
    if (objParam.limit !== null && objParam.limit !== undefined && objParam.limit !== "") {
        objParam.limit = objParam.limit;
    } else {
        objParam.limit = 200;
    }
    if (objParam.offset !== null && objParam.offset !== undefined && objParam.offset !== "") {
        objParam.offset = objParam.offset;
    } else {
        objParam.offset = 0;
    }

    Message.find({
        senderid: {
            $in: [objParam.senderId, objParam.receiverId]
        },
        receiverid: {
            $in: [objParam.senderId, objParam.receiverId]
        }
    }).select({ "message": 1, "senderid": 1, "receiverid": 1, "isdeleted": 1, "_id": 0 })
        .sort({
            _id: 1
        }).then(function (response) {
            if (response.length > 0) {
                u.each(response, (userData) => {
                    userData.message = jwt.decode(userData.message, TokenKey);
                });
                response.sort((a, b) => b.created_at - a.created_at);
                res.json({
                    success: true,
                    data: response
                }, 200);
            } else {
                res.json({
                    success: true,
                    data: []
                }, 200);
            }
        }).catch(function (err) {
            res.json({
                success: false,
                message: err.message
            }, 500);
        });
});

module.exports = router;