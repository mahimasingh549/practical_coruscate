const mongoose = require("mongoose"),
    Schema = mongoose.Schema;

const schemaOptions = {
    timestamps: {
        createdAt: "created_at",
        updatedAt: "last_updated",
    },
    versionKey: false,
};

let tblmessageSchema = new Schema(
    {
        senderid: {
            type: String,
        },
        receiverid: {
            type: String,
        },
        message: {
            type: String,
        },
        isdeleted: {
            type: Number,
            default: 0,
        }
    });
var tblmessage = mongoose.model("tblmessage", tblmessageSchema);
module.exports = tblmessage;
