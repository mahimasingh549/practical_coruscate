const mongoose = require("mongoose"),
    Schema = mongoose.Schema;

const schemaOptions = {
    timestamps: {
        createdAt: "created_at",
        updatedAt: "last_updated",
    },
    versionKey: false,
};

let tbluserSchema = new Schema(
    {
        id: {
            type: String,
        },
        name: {
            type: String,
        },
        online: {
            type: Number,
            default: 0,
        }
    });
var tbluser = mongoose.model("tbluser", tbluserSchema);
module.exports = tbluser;
